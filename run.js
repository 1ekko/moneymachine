import puppeteer from 'puppeteer'
import dappeteer from 'dappeteer'
import * as cheerio from 'cheerio';
import request from 'request';
import md5 from 'md5';
import axios from 'axios';
import async from 'async';
import Xvfb from 'xvfb';
var xvfb = new Xvfb({
    silent: true,
    xvfb_args: ["-screen", "0", '1280x720x24', "-ac"],
});
xvfb.start((err)=>{if (err) console.error(err)})

const browser = await dappeteer.launch(puppeteer,{
	headless:true,
	args: ['--no-sandbox', '--start-fullscreen', '--display='+xvfb._display]
})
var pages=[];
var startTime=new Date().getTime();
async function waitFor(page,string,timeout){
		return new Promise(resolve => {
			var start=new Date().getTime();
			async function check(resolve,page,string,timeout){
				var text=await page.content();
				if(text.match(new RegExp(string[0],string[1]))){
					resolve();
	   		}else{
	   			var current=new Date().getTime();
	   			var diff=(current-start)/1000;
	   			if(diff<timeout){
		   			setTimeout(function(){
		   				check(resolve,page,string,timeout)
		   			},1000)
		   		}else{
		   			console.log('timeout after ['+timeout+']')
		   			resolve();
		   		}
	   		}
		}
		check(resolve,page,string,timeout)
   	})
}
function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }
function parseContent(text,opts){
	try{
		if(!text) return false;
		var sp=text.split('<br>');
		var index=0;
		var startIndex=0;
		var data={}
		var exchangeLink='';
		var p=opts.pageInfo.split(/\n/);
		var tp=p[1].split(': ');
		exchangeLink=tp[1];
		var urld=new URL(exchangeLink);
		var exchangeDomain=urld.hostname.replace('www.','');
		console.log('exchange link: '+exchangeLink+ ' ('+exchangeDomain+')');
		for (var i = 0; i < sp.length; i++) {
			var line=sp[i];
			const $ = cheerio.load(line);
			if(line.indexOf('- <a')>=0){
				index++;
				startIndex=i;
			}
			if(index>0){
				if(!data[index]) data[index]={}
				if(startIndex==i){//first line
					//1 - <a href="https://info.apeswap.finance/pair/0xDd5F9a9f05a9680867936C878Ae6Db4C9Dab5C37" target="_blank">[SING]-[WBNB] APE LP</a> <a href="https://app.apeswap.finance/add/0x23894C0ce2d79B79Ea33A4D02e67ae843Ef6e563/0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c" target="_blank">[+]</a> <a href="https://app.apeswap.finance/remove/0x23894C0ce2d79B79Ea33A4D02e67ae843Ef6e563/0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c" target="_blank">[-]</a> <a href="https://app.apeswap.finance/swap?inputCurrency=0x23894C0ce2d79B79Ea33A4D02e67ae843Ef6e563&amp;outputCurrency=0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c" target="_blank">[&lt;=&gt;]</a> Price: $115.72 TVL: $187,421.9
					data[index].tokenLink=$('a').first().attr('href');
					var pp=line.split('Price: ');
					if(pp[1].indexOf('TVL')>=0){
						var tp=pp[1].split(' TVL: ');
						data[index].volume=parseFloat(tp[1].replace('$','').replace(',',''))
					}else{
						var tp=pp[1].split(' Market Cap: ');
						if(tp[1]) var np=tp[1].split(' <a');
						else var np=[tp[0]];
						data[index].marketCap=parseFloat(np[0].replace('$','').replace(',',''));
						//add token
						//console.log(pp[0])
						const $2 = cheerio.load(pp[0]);
						data[index].tokens=[
							$2('a').first().text()
						]
					}
					data[index].page=opts.page;
					data[index].pageInfo=escapeHtml(opts.pageInfo);
					data[index].price=parseFloat(tp[0].replace('$','').replace(',',''));
					data[index].exchangeLink=exchangeLink;
					data[index].exchangeDomain=exchangeDomain;
					data[index].id=md5(data[index].tokenLink);

				}else if(line.indexOf('Price')>=0){
					//SING Price: $4.86
					if(!data[index].tokens) data[index].tokens=[];
					if(!data[index].prices) data[index].prices={};
					var pp=line.split(' Price: $');
					data[index].tokens.push(pp[0]);
					data[index].prices[pp[0]]=parseFloat(pp[1].replace('$','').replace(',',''))
				}else if(line.indexOf('Staked')>=0){
					//'Staked: 40064.9831 SING ($194,895.79)',
					//var pp=line.split(' Staked: $');
				}else if(line.indexOf('APR')>=0){
					//APR: Day 5.32% Week 37.24% Year 1936.74%
					//APR: Day NaN% Week NaN% Year NaN%
					var dp=line.split('Day ');
					var wa=dp[1].split(' Week ');
					var yp=wa[1].split(' Year ');
					if(wa[0]=='NaN%'||wa[0]=='Infinity%'||wa[0]=='Infinity'){
						data[index].apr={
							day:0,
							week:0,
							year:0
						}
					}else{
						data[index].apr={
							day:parseFloat(wa[0].replace('%','').replace(',','')),
							week:parseFloat(yp[0].replace('%','').replace(',','')),
							year:parseFloat(yp[1].replace('%','').replace(',',''))
						}
					}
				}
			}
		}
		return data;
	}catch(e){
		console.log('error parsing');
		console.log(e)
	}
}
async function main() {
  console.log('Browser created');
  console.log('Linking metamask account');
  const metamask = await dappeteer.getMetamask(browser,{
  	debug:1,
  	seed:'crawl deal kidney legal lemon humor coast dentist absurd pink upper modify',
  	password:'snNK44#lAh@Ab4m2'
  })
  console.log('Adding BSC network');
  // you can change the network if you want
  //await metamask.switchNetwork('ropsten')
  await metamask.addNetwork({
  	name:'',
  	url:'https://bsc-dataseed.binance.org/',
  	chainID:'56',
  	blockUrl:'https://bscscan.com'
  });
  	//await metamask.close();
  // go to a dapp and do something that prompts MetaMask to confirm a transaction
  //connect VFAT to Metamask
	return true;
}
async function loadTestPage(){
	await loadPage('https://vfat.tools/bsc/pancake/');
}
async function loadPages(){
	console.log('loading page directory')
	const vfat = await browser.newPage();
	await vfat.goto('https://vfat.tools/bsc/');
	await waitFor(vfat,['Pool Provider','gi'],60);
	let element = await vfat.$('#log')
	let content = await vfat.evaluate(el => el.innerHTML, element)
	const $ = cheerio.load(content);
	var links = $('a'); //jquery get all hyperlinks
	$(links).each(function(i,link){
		pages.push('https://vfat.tools/bsc/'+$(link).attr('href'))
	})
	vfat.close()
}
async function saveData(data){
	return new Promise(resolve => {//API here
		axios
		  .post('https://api.phijs.earth/moneymachine/save', {
		    data: data
		  })
		  .then(res => {
		    console.log(res.data)
		    resolve();
		  })
		  .catch(error => {
		    console.error(error)
		  	resolve();
		  })
	})
}
async function loadPage(page){
	const vfat = await browser.newPage()
	//start with a page we know, and a page we know loads quick!
	await vfat.goto(page);
   await waitFor(vfat,['Finished reading smart contracts','gi'],90);
   let element = await vfat.$('#log')
	let content = await vfat.evaluate(el => el.innerHTML, element)
   var p=content.split('Finished reading smart contracts');
	var pi=p[0].split('<a href="#" id="connect_wallet_button">[CHANGE WALLET]</a>');
  	await saveData(parseContent(p[1],{
  		page:page,
  		pageInfo:pi[0]
  	}));
   await vfat.close();
   return true;
}
async function linkMetaMask(){
	console.log('linking metamask')
	const vfat = await browser.newPage()
	//start with a page we know, and a page we know loads quick!
	await vfat.goto('https://vfat.tools/bsc/singular/');
  	const linkWallet = await vfat.waitForSelector('#connect_wallet_button');
   await linkWallet.click();
   const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page()))); 
   const metamaskLink = await vfat.waitForSelector('.web3modal-provider-wrapper:first-child');
   await metamaskLink.click();
   const popup = await newPagePromise;
   const next=await popup.waitForSelector('.permissions-connect-choose-account__bottom-buttons button:last-child');
   await next.click();
   const done=await popup.waitForSelector('.page-container__footer button:last-child');
   await done.click();
   await waitFor(vfat,['Finished reading smart contracts','gi'],60);
   let element = await vfat.$('#log')
	let content = await vfat.evaluate(el => el.innerHTML, element)
   var p=content.split('Finished reading smart contracts');
   var tp=p[1].split('<br>');
   await vfat.close();
   return true;
}
await main();
await linkMetaMask();
// await loadTestPage();
// process.exit(0)
//return false;
await loadPages();
//make quueue
var queue=async.queue(async function (opts, fin) {
    console.log('===> loading page ['+(opts.index+1)+'] of ['+opts.total+'] ['+opts.page+']')
	await loadPage(opts.page);
	var endTime=new Date().getTime();
	var diff=(endTime-startTime)/1000
	var minutes=Math.floor(diff/60);
	var sec=Math.floor(diff-(60*minutes))
	console.log('Run Time: ['+minutes+'] minutes ['+sec+'] seconds');
	fin()
}, 2)
queue.drain(function(){
	process.exit(0);
})
for (var i = 0; i < pages.length; i++) {
	var page=pages[i];
	//if(i<3){
		queue.push({
			index:i,
			total:pages.length,
			page:page
		})
	//}
}
